#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import codecs
from optparse import OptionParser
import string

import corpus2

from ngrammodel import NGramModel
import tagplaintext

_stream = sys.stdout

class Mode:
	PLAINTEXT = 0
	TAGGED = 1

def chunks(reader):
	while True:
		chunk = reader.get_next_chunk()
		if not chunk:
			break;
		yield chunk

def czy_orzeczenie(token):
	klasa=token['ctag'].split(":")[0]
	return klasa in ['fin','bedzie','praet','impt','pcon','pant','winien']
		
def czy_wolacz(token):
	return ':voc:' in token['ctag']

def is_aglt(ctag):
	return 'aglt' in ctag	

def is_qub(ctag):
	return 'qub' in ctag	

def is_prep(ctag):
	return 'prep' in ctag

def contains_tag(tag, ctag):
	return tag in ctag

def match_phrase(sentence,phrase):
	matches = True
	words = phrase.split(' ')
	for j in range(len(words)):
		if (j>=len(sentence)) or (sentence[j]['base']<>words[j] and sentence[j]['orth']<>words[j]):
			matches = False
		# if (j>=len(sentence)) or sentence[j]['orth']<>words[j]:
		# 	matches = False
	return matches

def insert_punctuation(file_path, mode):
	wprowadzajace = ['aby','acz','aczkolwiek','albowiem','azali','aż','ażeby','bo','boć','bowiem','by','byle','byleby','chociaż','chociażby','choć','choćby','chybaby','chyba że','chyba żeby','co','cokolwiek','czy','czyj','dlaczego','dlatego','dlatego że','dokąd','dokądkolwiek','dopiero','dopiero gdy','dopóki','gdy','gdyby','gdyż','gdzie','gdziekolwiek','ile','ilekolwiek','ilekroć','ile razy','ile że','im','iż','iżby','jak','jakby','jak gdyby','jaki','jakikolwiek','jakkolwiek','jako','jakoby','jako że','jakżeby','jeśli','jeśliby','jeżeli','jeżeliby','kędy','kiedy','kiedykolwiek','kiedyż','kto','ktokolwiek','którędy','który','ledwie','ledwo','mimo iż','mimo że','na co','niech','nim','odkąd','o ile','po co','po czym','podczas gdy','pomimo iż','pomimo że','ponieważ','póki','przy czym','skąd','skądkolwiek','skoro','tak jak','tylko że','tyle że','tym bardziej że','w miarę jak','wprzód nim','w razie gdyby','za co','zaledwie','zanim','zwłaszcza gdy','zwłaszcza jeżeli','zwłaszcza kiedy','zwłaszcza że','że aż','że','żeby']

	#zdania współrzędne
	przeciwstawne = ['a', 'ale', 'alści', 'jednak', 'jednakże', 'jedynie', 'lecz', 'natomiast', 'przecież', 'raczej', 'tylko że', 'tymczasem', 'wszakże', 'zaś', 'za to'] #tylko
	wynikowe = ['więc', 'dlatego', 'toteż', 'zatem', 'wobec tego', 'skutkiem tego', 'wskutek tego', 'przeto', 'tedy'] #stąd
	synonimiczne = ['czyli', 'to znaczy']
	sumpws = przeciwstawne + wynikowe + synonimiczne

	laczne = ['i', 'oraz', 'tudzież', 'i zarazem']
	rozlaczne = ['lub', 'albo', 'bądź', 'czy']
	wylaczajace = ['ani', 'ni']
	sumlrw = laczne + rozlaczne + wylaczajace
	
	spojniki = przeciwstawne + wynikowe + synonimiczne + laczne + rozlaczne + wylaczajace

	typowe = ['albo raczej', 'czy raczej', 'lub raczej', 'albo lepiej', 'lub lepiej', 'ani też', 'ani nawet', 'czy może']

	#wczytanie danych z chunkera
	sentences = []
	plaintext = None
	if mode == Mode.PLAINTEXT:
		plaintext, sentences = tagplaintext.tag_plaintext(file_path)
	elif mode == Mode.TAGGED:
		sentences = tagplaintext.read_tagged_file(file_path)
	else:
		raise Exception("Unsupported punctuation mode.")
	
	#model językowy
	model = NGramModel()
	model.read_lm("ijn-model.lm")
	
	#robimy przecinki
	przecinki=[]
	
	for zd in range(len(sentences)):
		pomin = 0
		sentence = sentences[zd]
		przecinki.append([])
		powtorzenia = []
		orze = []
		for i in range(len(sentence)):
			if pomin>0:
				pomin -= 1
			else:
				# 1. wyrazenia wprowadzajace zd. podrzędne
				rule1=0
				for phrase in wprowadzajace:
					if match_phrase(sentence[i:],phrase):
						rule1 = i
						pomin = len(phrase.split(' '))-1
						#wyjatek a) cofamy przecinek przed przyimek
						tagclass = sentence[i-1]['ctag'].split(':')[0]
						if (tagclass in ['qub', 'prep']) and sentence[i-1]['orth'] != 'się':
							#print sentence[i-1]['orth'], sentence[i]['orth'], sentence[i+1]['orth']
							rule1 -= 1
						#wyjatek b) nie stawiamy, jesli byl spojnik, np. "i który"
						pre = sentence[rule1-1]['base']
						if pre in spojniki:
							rule1 = 0
							pomin = 0
				if rule1>0:
					przecinki[zd].append(rule1)
				# 2. spójniki współrzędne
				rule2=0
				# a) z przecinkiem: przeciwstawne, wynikowe, synonimiczne
				for phrase in sumpws:
					if match_phrase(sentence[i:],phrase):
						rule2 = i
						pomin = len(phrase.split(' '))-1
				# b) z przecinkiem: typowe wyrazenia z łącznymi i rozłącznymi
				for phrase in typowe:
					if match_phrase(sentence[i:],phrase):
						rule2 = i
						pomin = len(phrase.split(' '))-1
				if rule2>0:
					przecinki[zd].append(rule2)
				# 3. Obustronnie wydzielamy cały wołacz ", mój drogi Stefanie, "
				if czy_wolacz(sentence[i]):
					if i>0 and not czy_wolacz(sentence[i-1]):
						przecinki[zd].append(i)
					if i+1<len(sentence) and not czy_wolacz(sentence[i+1]):
						przecinki[zd].append(i+1)
				# 4. Obustronnie: typowe wtrącenia
				for phrase in ['innymi słowy','ściśle mówiąc', 'tak czy owak', 'być może']:
					if match_phrase(sentence[i:],phrase):
						przecinki[zd].append(i)
						przecinki[zd].append(i+len(phrase.split(' ')))
						pomin = len(phrase.split(' '))-1
				# 5. Okrzyk ach, hej, halo, ho, oj
				if sentence[i-1]['orth'].lower() in ['ach','hej','halo','ho','oj']:
					przecinki[zd].append(i)
				# 6. Powtórzone spójniki bezprzecinkowe i przyimki
				if (i in przecinki[zd] or \
					(sentence[i]['base'] in sumlrw and sentence[i]['base'] not in powtorzenia)):
					powtorzenia = []
				if is_prep(sentence[i]['ctag']) or sentence[i]['base'] in sumlrw:
					if sentence[i]['base'] in powtorzenia:
						przecinki[zd].append(i)
						powtorzenia = []
					powtorzenia.append(sentence[i]['base'])
				# 7. Szukanie orzeczeń
				if czy_orzeczenie(sentence[i]):
					# dwa orzeczenia przy sobie oddzielamy odrazu
					if (len(orze)>0 and i-orze[-1] == 1): 
						przecinki[zd].append(i)
					elif (i>0 and len(orze)>0 and sentence[i-1]['base']=='nie' and i-orze[-1] == 2):
						przecinki[zd].append(i-1)
					orze.append(i)

		#usuwamy duplikaty w przecinkach
		przecinki[zd] = list(set(przecinki[zd]))
		przecinki[zd].sort()
		
		#
		# 8. Między orzeczeniami - użycie modelu probabilistycznego
		#
		#zbieramy odcinki wyznaczone przecinkami i spojnikami
		tmp = set(przecinki[zd][:])
		for i in range(len(sentence)):
			if sentence[i]['base'] in sumlrw:
				tmp.add(i)
		tmp = list(tmp)
		tmp.sort()
		tmp.append(len(sentence)+1)
		tmp.insert(0,0)
		
		# Ta pętla przechodzi po odcinkach wyznaczonych przecinkami i spojnikami
		for k in range(len(tmp)-1):
			odcinek = sentence[tmp[k]:tmp[k+1]]
			if len(odcinek)>=3:
				orze2 = []
				for o in orze:
					if tmp[k]<=o and o<tmp[k+1]:
						orze2.append(o)
				while len(orze2)>=2:	#dla każdej pary orzeczeń w tym odcinku
					first = orze2.pop(0)
					second = orze2[0]
					probs = []
					# dla każdych 2 słów między parą orzeczeń (first i second) odpalanie modelu
					for f in range(first,second):
						w1 = sentence[f-1]
						w2 = sentence[f]
						logprob = model.logprob_sequence(",", [w1['ctag'], w2['ctag']] )
						w1 = sentence[f]
						w2 = sentence[f+1]
						logprob += model.logprob_sequence(w2['ctag'], [w1['ctag'], ","] )
						if f + 2 < len(sentence):
							w1 = sentence[f+1]
							w2 = sentence[f+2]
							logprob += model.logprob_sequence(w2['ctag'], [",", w1['ctag']] )
						probs.append(logprob)
					best = probs.index(max(probs))
					print sentence[first]['orth'], sentence[second]['orth']
					print zd, best, max(probs)
					print probs
					przecinki[zd].append(first+best+1)
					
					
		# 9. Wyliczenia - to można by jeszcze ulepszyć
		for i in range(len(sentence)): #taki sam tag = wyliczanie
			if sentence[i-1]['ctag'][:10]==sentence[i]['ctag'][:10]\
				and not sentence[i-1]['orth'][0].isupper()\
				and not (contains_tag('gen', sentence[i]['ctag'])and contains_tag('subst', sentence[i]['ctag']))\
				and sentence[i-1]['base'] not in ['ten','mój'] \
				and not contains_tag('conj', sentence[i-1]['ctag']):
				przecinki[zd].append(i)

				
	
	#sklejenie wyjscia
	wyjscie = ''
	if plaintext is not None: #z plaintextu
		przecinki2 = []
		nr = 0
		for zd in range(len(sentences)):
			sentence = sentences[zd]
			for i in range(len(sentence)):
				token=sentence[i]
				if i in przecinki[zd]:
					przecinki2.append(nr)
				nr += len(token['orth'].decode('utf-8'))
		nr = 0
		for char in plaintext.decode('utf-8'):
			if nr in przecinki2:
				przecinki2.pop(0)
				if (char==' '):
					wyjscie += ','
			wyjscie += char
			if char.isalnum():
				nr+=1
	else: #z otagowanego
		for zd in range(len(sentences)):
			sentence = sentences[zd]
			for i in range(len(sentence)):
				token=sentence[i]
				if i in przecinki[zd]:
					wyjscie+= ','
				wyjscie+= ' '
				wyjscie+= token['orth']
		wyjscie = wyjscie.replace(" .",".").strip()

	return wyjscie

def evaluate(result, correct):
	cor = [word.strip() for line in codecs.open(correct, 'r', encoding='utf-8') for word in line.split(' ')]
	if u'' in cor:
		cor.remove(u'')
	res = result.split(" ")
	res = [x.split('\n') for x in res if x]
	res = [x for sub in res for x in sub]
	if u'' in res:
		res.remove(u'')

	# miary true positive, true negative false positive oraz false negative
	tp = 0
	tn = 0
	fp = 0
	fn = 0

	for i in xrange(len(cor)):
		res_tok = res[i]
		cor_tok = cor[i]

		if u',' in cor_tok and u',' in res_tok:
			tp += 1
		if not u',' in cor_tok and not u',' in res_tok:
			tn += 1
		if u',' in cor_tok and not u',' in res_tok:
			fn += 1
		if not u',' in cor_tok and u',' in res_tok:
			fp += 1

	print "Actual class / predicted class"
	print "tp , fp \\n fn, tn"
	print string.rjust(`tp`, 4), string.rjust(`fp`, 4)
	print string.rjust(`fn`, 4), string.rjust(`tn`, 4)
	print "Precision \\n recall"
	print float(tp) / float(tp + fp)
	print float(tp) / float(tp + fn)
		

def main():
	parser = OptionParser()
	parser.add_option("-p", "--plaintext", dest="plaintext",
		help="Add punctuation to plaintext FILE", metavar="FILE")
	parser.add_option("-t", "--tagged", dest="taggedfile",
		help="Add punctuation to tagged FILE", metavar="FILE")
	parser.add_option("-o", "--out", help="Write to OUT instead of stdout")
	parser.add_option("-e", "--evaluate", metavar="FILE",
		help="Evaluate with respect to correct punctuation in FILE")
	(options, args) = parser.parse_args()

	if not options.plaintext and not options.taggedfile:
		print "No input file specified, punctuation failed..."
		sys.exit(2)

	if options.plaintext and options.taggedfile:
		print "Only one input file is allowed, punctuation failed..."
		sys.exit(2)

	global _stream
	if options.out:
		_stream = codecs.open(options.out, 'w', encoding='utf-8')

	result = None
	if options.plaintext:
		result = insert_punctuation(options.plaintext, Mode.PLAINTEXT)

	if options.taggedfile:
		result = insert_punctuation(options.taggedfile, Mode.TAGGED)

	if not result is None:
		_stream.write(result + "\n")

	if options.evaluate:
		evaluate(result, options.evaluate)

	if options.out:
		_stream.close()

if __name__ == "__main__":
	main()
