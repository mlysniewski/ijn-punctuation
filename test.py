#!/usr/bin/env python
# -*- coding: utf-8 -*-

from ngrammodel import NGramModel
import tagplaintext
import corpus2

_tagset = "nkjp"
_input_format = "xces"

def chunks(reader):
	while True:
		chunk = reader.get_next_chunk()
		if not chunk:
			break;
		yield chunk

def czy_orzeczenie(token):
	klasa=token['ctag'].split(":")[0]
	return klasa in ['fin','bedzie','praet','impt','pcon','pant','winien']
		
def czy_wolacz(token):
	return ':voc:' in token['ctag']

def is_aglt(ctag):
	return 'aglt' in ctag	

def is_qub(ctag):
	return 'qub' in ctag	

def is_prep(ctag):
	return 'prep' in ctag

def contains_tag(tag, ctag):
	return tag in ctag

def match_phrase(sentence,phrase):
	matches = True
	words = phrase.split(' ')
	for j in range(len(words)):
		if (j>=len(sentence)) or (sentence[j]['base']<>words[j] and sentence[j]['orth']<>words[j]):
			matches = False
	return matches

def read_data(path):
	tagset = corpus2.get_named_tagset(_tagset)
	reader = corpus2.TokenReader.create_path_reader(_input_format, tagset, path)
	zdania = []
	for chunk in chunks(reader):
		for sent in chunk.sentences():
			zdanie=[]
			for tok in sent.tokens():
				token = {}
				token['orth'] = tok.orth_utf8()
				token['base'] = tok.lexemes()[0].lemma_utf8()
				token['ctag'] = tagset.tag_to_string(tok.lexemes()[0].tag())
				if token['ctag']=='interp':
					pass
				elif is_aglt(token['ctag']): # Chciał em : doklejamy koncowke do poprzedniego tokenu
					zdanie[-1]['orth'] += token['orth']
				else:
					zdanie.append(token)
			zdania.append(zdanie)
	return zdania

def main():
	wprowadzajace = ['aby','acz','aczkolwiek','albowiem','azali','aż','ażeby','bo','boć','bowiem','by','byle','byleby','chociaż','chociażby','choć','choćby','chybaby','chyba że','chyba żeby','co','cokolwiek','czy','czyj','dlaczego','dlatego','dlatego że','dokąd','dokądkolwiek','dopiero','dopiero gdy','dopóki','gdy','gdyby','gdyż','gdzie','gdziekolwiek','ile','ilekolwiek','ilekroć','ile razy','ile że','im','iż','iżby','jak','jakby','jak gdyby','jaki','jakikolwiek','jakkolwiek','jako','jakoby','jako że','jakżeby','jeśli','jeśliby','jeżeli','jeżeliby','kędy','kiedy','kiedykolwiek','kiedyż','kto','ktokolwiek','którędy','który','ledwie','ledwo','mimo iż','mimo że','na co','niech','nim','odkąd','o ile','po co','po czym','podczas gdy','pomimo iż','pomimo że','ponieważ','póki','przy czym','skąd','skądkolwiek','skoro','tak jak','tylko że','tym bardziej że','w miarę jak','wprzód nim','w razie gdyby','za co','zaledwie','zanim','zwłaszcza gdy','zwłaszcza jeżeli','zwłaszcza kiedy','zwłaszcza że','że aż','że','żeby']
	przeciwstawne = ['a', 'ale', 'alści', 'jednak', 'jednakże', 'jedynie', 'lecz', 'natomiast', 'przecież', 'raczej', 'tylko że', 'tymczasem', 'wszakże', 'zaś', 'za to'] #tylko
	wynikowe = ['więc', 'dlatego', 'toteż', 'zatem', 'wobec tego', 'skutkiem tego', 'wskutek tego', 'przeto', 'tedy'] #stąd
	synonimiczne = ['czyli', 'to znaczy']
	laczne = ['i', 'oraz', 'tudzież', 'i zarazem']
	rozlaczne = ['lub', 'albo', 'bądź', 'czy']
	wylaczajace = ['ani', 'ni']
	typowe = ['albo raczej', 'czy raczej', 'lub raczej', 'albo lepiej', 'lub lepiej', 'ani też', 'ani nawet', 'czy może']
	#pomocnicze listy spójników
	sumpws = przeciwstawne + wynikowe + synonimiczne
	sumlrw = laczne + rozlaczne + wylaczajace
	spojniki = przeciwstawne + wynikowe + synonimiczne + laczne + rozlaczne + wylaczajace

	
	#wczytanie danych z chunkera
	sentences = read_data("./dane6.xml")
	
	#model językowy
	model = NGramModel()
	model.read_lm("ijn-model.lm")
	
	#robimy przecinki
	przecinki=[]
	
	for zd in range(len(sentences)):
		pomin = 0
		sentence = sentences[zd]
		przecinki.append([])
		powtorzenia = []
		orze = []
		for i in range(len(sentence)):
			if pomin>0:
				pomin -= 1
			else:
				# 1. wyrazenia wprowadzajace zd. podrzędne
				rule1=0
				for phrase in wprowadzajace:
					if match_phrase(sentence[i:],phrase):
						rule1 = i
						pomin = len(phrase.split(' '))-1
						#wyjatek a) cofamy przecinek przed przyimek
						tagclass = sentence[i-1]['ctag'].split(':')[0]
						if tagclass in ['qub', 'prep']:
							rule1 -= 1
						#wyjatek b) nie stawiamy, jesli byl spojnik, np. "i który"
						pre = sentence[rule1-1]['base']
						if pre in spojniki:
							rule1 = 0
							pomin = 0
				if rule1>0:
					przecinki[zd].append(rule1)
				# 2. spójniki współrzędne
				rule2=0
				# a) z przecinkiem: przeciwstawne, wynikowe, synonimiczne
				for phrase in sumpws:
					if match_phrase(sentence[i:],phrase):
						rule2 = i
						pomin = len(phrase)-1
				# b) z przecinkiem: typowe wyrazenia z łącznymi i rozłącznymi
				for phrase in typowe:
					if match_phrase(sentence[i:],phrase):
						rule2 = i
						pomin = len(phrase)-1
				if rule2>0:
					przecinki[zd].append(rule2)
				# 3. Obustronnie wydzielamy cały wołacz ", mój drogi Stefanie, "
				if czy_wolacz(sentence[i]):
					if i>0 and not czy_wolacz(sentence[i-1]):
						przecinki[zd].append(i)
					if i<len(sentence) and not czy_wolacz(sentence[i+1]):
						przecinki[zd].append(i+1)
				# 4. Obustronnie: typowe wtrącenia
				for phrase in ['innymi słowy','ściśle mówiąc']:
					if match_phrase(sentence[i:],phrase):
						przecinki[zd].append(i)
						przecinki[zd].append(i+len(phrase))
						pomin = len(phrase)-1
				# 5. Okrzyk ach, hej, halo, ho, oj
				if sentence[i-1]['base'] in ['ach','hej','halo','ho','oj']:
					przecinki[zd].append(i)
				# 6. Powtórzone spójniki bezprzecinkowe i przyimki
				if (i in przecinki[zd] or \
					(sentence[i]['base'] in sumlrw and sentence[i]['base'] not in powtorzenia)):
					powtorzenia = []
				if is_prep(sentence[i]['ctag']) or sentence[i]['base'] in sumlrw:
					if sentence[i]['base'] in powtorzenia:
						przecinki[zd].append(i)
						powtorzenia = []
					powtorzenia.append(sentence[i]['base'])
				# 7. Szukanie orzeczeń
				if czy_orzeczenie(sentence[i]):
					# dwa orzeczenia przy sobie oddzielamy odrazu
					if (len(orze)>0 and i-orze[-1] == 1): 
						przecinki[zd].append(i)
					elif (i>0 and len(orze)>0 and sentence[i-1]['base']=='nie' and i-orze[-1] == 2):
						przecinki[zd].append(i-1)
					orze.append(i)
				# 8. Wyliczenia - to można jeszcze ulepszyć
				if sentence[i-1]['ctag'][:10]==sentence[i]['ctag'][:10]: #taki sam tag = wyliczanie
					przecinki[zd].append(i)
				
		#usuwamy duplikaty w przecinkach
		przecinki[zd] = list(set(przecinki[zd]))
		przecinki[zd].sort()
		
		#
		# 9. Między orzeczeniami - użycie modelu probabilistycznego
		#
		#zbieramy odcinki wyznaczone przecinkami i spojnikami
		tmp = przecinki[zd][:]
		for i in range(len(sentence)):
			if sentence[i]['base'] in sumlrw:
				tmp.append(i)
		tmp.sort()
		tmp.append(len(sentence)+1)
		tmp.insert(0,0)
		
		# Ta pętla przechodzi po odcinkach wyznaczonych przecinkami i spojnikami
		for k in range(len(tmp)-1):
			odcinek = sentence[tmp[k]:tmp[k+1]]
			if len(odcinek)>=3:
				orze2 = []
				for o in orze:
					if tmp[k]<=o and o<tmp[k+1]:
						orze2.append(o)
				while len(orze2)>=2:	#dla każdej pary orzeczeń w tym odcinku
					first = orze2.pop(0)
					second = orze2[0]
					probs = []
					# dla każdych 2 słów między parą orzeczeń (first i second) odpalanie modelu
					for f in range(first,second):
						w1 = sentence[f-1]
						w2 = sentence[f]
						logprob = model.logprob_sequence(",",[w1['ctag'],w2['ctag']] )
						w1 = sentence[f]
						w2 = sentence[f+1]
						logprob = model.logprob_sequence(w2['ctag'],[w1['ctag'],","] )
						w1 = sentence[f+1]
						w2 = sentence[f+2]
						logprob += model.logprob_sequence(",",[w1['ctag'],w2['ctag']] )
						probs.append(logprob)
					best = probs.index(max(probs))
					print [probs,best]
					przecinki[zd].append(first+best+1)
	
	#sklejenie wyjscia
	wyjscie = ''
	for zd in range(len(sentences)):
		sentence = sentences[zd]
		for i in range(len(sentence)):
			token=sentence[i]
			if i in przecinki[zd]:
				wyjscie+= ','
			wyjscie+= ' '
			wyjscie+= token['orth']
				
	wyjscie = wyjscie.replace(" .",".").strip()
	print wyjscie

if __name__ == "__main__":
	main()
