#!/usr/bin/env python
# -*- coding: utf-8 -*-

import corpus2
import os
import sys
import optparse

_str_tagset = "nkjp"
_input_format = "xces"
_tagset = corpus2.get_named_tagset(_str_tagset)

def chunks(reader):
	while True:
		chunk = reader.get_next_chunk()
		if not chunk:
			break;
		yield chunk

def files_in_dir(dir):
	file_list = []
	for root, subFolders, files in os.walk(dir):
		for file in files:
			if file.lower().endswith('.xml') and \
			not file.lower().endswith('.rel.xml'):
				file_list.append(os.path.join(root,file))
	return file_list

def file_sentences(file):
	global _tagset
	reader = corpus2.TokenReader.create_path_reader(_input_format, \
		_tagset, file)

	sentences = []
	for chunk in chunks(reader):
		for sent in chunk.sentences():
			sentence = []
			for tok in sent.tokens():
				token = {}
				token['orth'] = tok.orth_utf8()
				token['base'] = tok.lexemes()[0].lemma_utf8()
				token['ctag'] = _tagset.tag_to_string(tok.lexemes()[0].tag())
				sentence.append(token)
			sentences.append(sentence)

	return sentences

def save_bases(sentences, file):
	f = open(file, 'w')

	for sentence in sentences:
		for token in sentence:
			if token['ctag'] != 'interp':
				f.write(token['base'] + " ")
			elif token['ctag'] == 'interp' and token['base'] == ',':
				f.write(', ')
		f.write('\n')

	f.close()

def save_tags(sentences, file):
	f = open(file, 'w')

	for sentence in sentences:
		for token in sentence:
			if token['ctag'] != 'interp':
				f.write(token['ctag'] + " ")
			elif token['ctag'] == 'interp' and token['base'] == ',':
				f.write(', ')
		f.write('\n')

	f.close()

def main():
	p = optparse.OptionParser()
	p.add_option("-i", "--input", 
		help="Directory of corpus to be converted to SRILM acceptable format")
	p.add_option("-o", "--output",
		help="Directory of converted, SRILM acceptable corpus")
	p.add_option("-b", "--base", default=False, action="store_true",
		help="Save base forms and commas to converted corpus", dest="base")
	p.add_option("-t", "--tags", default=False, action="store_true",
		help="Save tags and commas to converted corpus", dest="tags")
	(opt, args) = p.parse_args()

	if not opt.input and not opt.output:
		print "Input and output directories are obligatory."
		sys.exit(1)

	if (not opt.base and not opt.tags) or (opt.base and opt.tags):
		print "Only --base or --tags are allowed."
		sys.exit(1)

	
	file_list = files_in_dir(opt.input)
	output_dir = opt.output
	if not os.path.exists(output_dir):
		os.makedirs(output_dir)

	for file in file_list:
		path, file_name = os.path.split(file)
		file_name = file_name.replace('.xml', '.txt')
		path_b = output_dir_b + file_name
		path_t = output_dir_t + file_name
		sentences = file_sentences(file)
		if (opt.tags):
			save_bases(sentences, path_b)
		elif opt.base:
			save_tags(sentences, path_t)

if __name__ == "__main__":
	main()