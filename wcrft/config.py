# -*- coding: utf-8 -*-
# This file is part of WCRFT
# Copyright (C) 2011 Adam Radziszewski, Paweł Orłowicz.
# WCRFT is free software; you can redistribute and/or modify it
# under the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation; either version 3 of the License, or (at your option)
# any later version.
#
# WCRFT is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.
#
# See the LICENCE and COPYING files for more details

# global section with tagset-related definitions
S_GLOBAL = 'general'
O_TAGSET = 'tagset'
O_FEATS = 'features'
O_MACACFG = 'macacfg'

# comma-separated list of tagset attributes, whose order will be translated into tagging layers
# NOTE: use attribute mnemonics as defined in corpus2 tagset (see .tagset file or run tagset-tool to check)
O_ATTRS = 'attrs'

# settings related to gathering of lexicon of frequent forms
S_LEXICON = 'lexicon'
O_CASESENS = 'casesens' # default: no
O_MINFREQ = 'minfreq' # default: 10
O_MAXENTRIES = 'maxentries' # default: 500

# tagging of unknown words
S_UNKNOWN = 'unknown'
O_UNKGUESS = 'guess' # shall we tag unknown forms?
O_UNKFREQ = 'unktagfreq' # gathering unk tags occurring at least this frequent


# attrname for wordclass
WORDCLASS = 'CLASS'

# WCCL file section defining ops for all layers
DEFAULT_OPS = 'default'

# file extensions
EXT_LEX = 'lex'
EXT_UNKTAGS = 'unk' # for storing unknown word tags
EXT_WCCL = 'ccl'
EXT_DATA = 'tab'
EXT_TEXT = 'txt'
EXT_CR = 'cr'

# classifier-specific options
S_CLASSIFIER = 'crf'
O_PARAMS = 'params'
