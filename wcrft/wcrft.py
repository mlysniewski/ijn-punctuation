#!/usr/bin/python
# -*- coding: utf-8 -*-
# This file is part of WCRFT
# Copyright (C) 2011 Adam Radziszewski, Paweł Orłowicz.
# WCRFT is free software; you can redistribute and/or modify it
# under the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation; either version 3 of the License, or (at your option)
# any later version.
#
# WCRFT is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.
#
# See the LICENCE and COPYING files for more details
import sys
from optparse import OptionParser

import tagger
import corpio

ioformats = corpio.format_help
ioformats = '\nINFO: formats: txt premorph; require installed Maca and Morfeusz' + ioformats.replace('input formats: ccl', 'input formats: txt premorph ccl')

descr = """%prog [options] CONFIGFILE [INPUT...]

WCRFT, Wroclaw CRF Tagger
(C) 2012, Wroclaw University of Technology

Tags input file(s) using the selected configuration. Use -d to specify where to
look for a trained tagger model (or where to store a model when training).

Use -O to specify output path (by default will write to stdout).
Use - to tag stdin to stdout.

When tagging multiple files, either give the filenames directly as arguments,
or use --batch and provide a filename to a list of paths. Either way, each file
will be tagged and the output writted to FILENAME.tag.

Training (--train) requires a configuration to use and a filename of the training
material. Trained model will be stored in DATA_DIR.
""" + ioformats


def lines(pathfilename):
	with open(pathfilename) as f:
		return [line.strip() for line in f if line.strip()]

def go():
	parser = OptionParser(usage=descr)
	parser.add_option('-i', '--input-format', type='string', action='store',
		dest='input_format', default='xces',
		help='set the input format; default: xces')
	parser.add_option('-o', '--output-format', type='string', action='store',
		dest='output_format', default='xces',
		help='set the output format; default: xces')
	parser.add_option('-O', '--output-file', type='string', action='store',
		dest='out_path', default='',
		help='set output filename (do not write to stdout)')
	parser.add_option('-d', '--data-dir', type='string', action='store',
		dest='data_dir', 
		help='assume WCCL and trained model to sit in the given dir')
	parser.add_option('-c', '--config', type='string', action='store',
		dest='maca_config', default='',
		help='overrides maca config file; default: morfeusz-nkjp')
	parser.add_option('-A', '--ambiguity', action='store_true',
		dest='preserve_amb',
		help='preserve non-disamb interpretations after tagging')
	parser.add_option('-C', '--chunks', action='store_true',
		dest='preserve_chunks', default=False,
		help='preserve input paragraph chunks (the default is to read sentences only)')
	parser.add_option('-v', '--verbose', action='store_true',
		dest='verbose', default=False,
		help='verbose mode')
	parser.add_option('--train', action='store_true',
		dest='is_training', help='train the tagger')
	parser.add_option('--batch', action='store_true',
		help='treat arguments as lists of paths to files to tag')
	(options, args) = parser.parse_args()
	
	if len(args) < 1:
		sys.stderr.write('You need to provide a config file and specify input.\n')
		sys.stderr.write('See %s --help\n' % sys.argv[0])
		sys.exit(1)
	config_path = args[0]
	files = args[1:]
	
	tagr = tagger.Tagger(config_path, options.data_dir,
		verbose = options.verbose)
	
	if options.maca_config != '':
		tagr.maca_config = options.maca_config

	if options.is_training:
		# tagger training
		assert len(files) == 1, 'must provide path to training file'
		tagr.train_and_save(files[0], options.input_format)
	else:
		# normal tagger performance
		inputs = []
		outputs = []
		if options.batch: # read each arg as input path list
			for pathpath in files:
				inputs.extend(lines(pathpath))
			outputs = [path + '.tag' for path in inputs]
		elif len(files) == 1:
			if files[0] == '-': # stdin to stdout
				inputs.append(None)
				outputs.append(None)
			else:
				inputs.append(files[0])
				outputs.append(options.out_path)
		else: # multiple paths as args
			inputs = files
			outputs = [path + '.tag' for path in inputs]
		tagr.load_model()
		for in_path, out_path in zip(inputs, outputs):
			if in_path and options.verbose:
				sys.stderr.write('Tagging %s...\n' % in_path)
			tagr.tag_input(in_path, out_path,
				options.input_format, options.output_format,
				options.preserve_chunks, options.preserve_amb)

if __name__ == '__main__':
	go()
