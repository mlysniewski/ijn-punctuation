# -*- coding: utf-8 -*-
# This file is part of WCRFT
# Copyright (C) 2011 Adam Radziszewski, Paweł Orłowicz. 
# WCRFT is free software; you can redistribute and/or modify it
# under the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation; either version 3 of the License, or (at your option)
# any later version.
#
# WCRFT is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.
#
# See the LICENCE and COPYING files for more details

__doc__ = """The actual tagger implementation."""

# SWIG bug workaround: loading multiple SWIG modules brought unwrapped
# swig::stop_iteration exceptions
import ctypes, sys
import platform
import os, codecs
import ConfigParser
from operator import itemgetter as ig
from collections import defaultdict as dd

if 'Linux' in platform.system():
	# this prevents from problems with multiple SWIG wrappers
	# (probably bug in SWIG) and possible problems with locating Maca plugin
	dlflags = sys.getdlopenflags()
	sys.setdlopenflags(dlflags | ctypes.RTLD_GLOBAL)

import corpio, config, classify

import corpus2

if 'Linux' in platform.system():
	# get back to default dlopen policy
	sys.setdlopenflags(dlflags)

DEFAULT_DIR = os.path.abspath(os.path.dirname(__file__))
DEFAULT_MODEL_DIR = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'model')
DEFAULT_CONFIG_DIR = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'config')
# TODO: get back to default dlopen policy?

class Stats:
	"""Statistics for reporting progress and diagnosis."""
	def __init__(self):
		self.clear()
	
	def clear(self):
		self.layer_gets = dd(int)
		self.layer_fails = dd(int)
		self.num_toks = 0
		self.num_sents = 0
		self.num_evals = 0 # classifier firing
	
	def dump(self):
		sys.stderr.write('Toks processed: %d\n' % self.num_toks)
		sys.stderr.write('Sents processed: %d\n' % self.num_sents)
		sys.stderr.write('Classifier calls: %d\n' % self.num_evals)
		sys.stderr.write('Layers (gets, fails):\n')
		for attr_name in sorted(self.layer_gets):
			num_gets = self.layer_gets[attr_name]
			num_fails = self.layer_fails[attr_name]
			if num_gets > 0:
				sys.stderr.write('\t%s:\t%d\t%d\n' % (attr_name, num_gets, num_fails))
		
	def maybe_report(self):
		if self.num_sents % 100 == 0:
			sys.stderr.write('%d toks, %d calls...\n' % (self.num_toks, self.num_evals))

class Tagger:
	"""The WMBT. The tagger performs morpho-syntactic disambiguation of already
	morphologically analysed input. In most cases the morphological information
	(i.e. assignment of possible interpretations to tokens) is kept intact.
	Depending on configuration used, forms assigned "unknown word" tags may be
	assigned tags from the "unknown tag list" gathered during training. This is
	performed initially and the rest of the tagging algorithm consists in
	disambiguation. Nevertheless, those "unknown" words are identified and
	treated with separate case bases (a separate case base is created for every
	layer, one for known words, the other for unknown).
	NOTE: lemmas are not guessed, token orths are assigned as lemmas to guessed
	interpretations. If this is important, please use external guesser.
	
	TODO: update
	The tagger performs tag selection, the output is guaranteed to contain
	exactly one tag per token. NOTE that multiple interpretations (lexemes) MAY
	still remain -- as one tag may occur with different lemmas. The information
	on lemmas may be used in the user-defined features (in a WCCL file).
	
	When training, the input must be morphologically analysed with the proper
	tags marked as "disamb". If there are multiple disamb tags, one is
	arbitrarily chosen ("first" according to the tagset definition).
	
	The tagger is parametrised with a config file that specifies Lesser General
	assumptions of the model (tagset, attributes, features) and a data dir that
	is to contain a specific trained model. The idea is that one config may be
	used to train several models using different corpora.
	
	To initialise a tagger for normal usage (performance), create a Tagger
	object and call load_model. The tagger will be ready to tag subsequent
	sentences -- by calling tag_sentence.
	For training, create a Tagger object and call train_and_save.
	
	Each sentence should be a corpus2.Sentence object.
	"""
	
	def __init__(self, config_path, data_dir, verbose = False):
		"""Creates a working tagger from given config (INI) filename.
		If config_path points to a config.ini file, there should be an
		accompanying file named config.ccl. Trained tagger model is sought in
		(or written to when training) data_dir. The model consists of a
		WCCL-compatible lexicon file (named config.lex) and trained classifiers
		for each tagset attribute."""
		self.data_dir = self.resolve_object_path(data_dir)
		config_path = self.resolve_object_path(config_path)
		self.conf_dir, conf_fname = os.path.split(config_path)
		# models (trained classifiers)
		self.model_name, dummy = os.path.splitext(conf_fname)
		self.verbose = verbose
		# load the config file
		with open(config_path) as config_file:
			self.conf = ConfigParser.RawConfigParser()
			self.conf.readfp(config_file)
		self.tagset = corpio.get_tagset(self.conf)
		# mask for unknown tags (actually, the unknown tag itself)
		self.unk_mask = self.tagset.make_ign_tag()
		# shall we guess unknown words' tags?
		self.treat_unknown = self.conf.getboolean(
			config.S_UNKNOWN, config.O_UNKGUESS)
		# uninitialised trained model
		self.layers = None
		self.models = None # attr_name -> timbl_api object
		self.unktags = None # list of corpus2.Tag objects
		self.stats = Stats()
		self.maca_config = self.conf.get(config.S_GLOBAL, config.O_MACACFG)

	
	def load_model(self):
		self.layers = corpio.Layers(self.conf, self.model_name,
			self.conf_dir, self.data_dir)
		self.models = {}
		for layer in self.layers.layers:
			attr_name = layer[0]
			self.models[attr_name] = classify.load(self.conf,
				self.model_name, self.data_dir, attr_name)
		if self.treat_unknown:
			# load unknown word tags
			with open(corpio.f_name(self.model_name, self.data_dir,
				config.EXT_UNKTAGS), 'r') as unk_file:
				self.unktags = [self.tagset.parse_simple_tag(line.strip())
					for line in unk_file if line.strip()]
	
	def _gather_lexicon(self, in_path, input_format):
		"""Reads in_path token by token, gathers most frequent forms and stores it
		in the form of a lexicon readable by WCCL expressions.
		Also gathers a list of most frequent tags of "unknown" words if
		unknown frequency threshold is above zero."""
		case_sens = self.conf.getboolean(config.S_LEXICON, config.O_CASESENS)
		min_freq = self.conf.getint(config.S_LEXICON, config.O_MINFREQ)
		max_entries = self.conf.getint(config.S_LEXICON, config.O_MAXENTRIES)
		unk_freq = self.conf.getint(config.S_UNKNOWN, config.O_UNKFREQ)
		
		if self.verbose:
			sys.stderr.write('Collecting forms for lexicon...\n')
		# gather all forms in the training data
		forms = {}
		unktags = {}
		reader = corpio.get_reader(in_path, self.tagset, input_format)
		while True:
			sent = reader.get_next_sentence()
			if not sent: # end of input
				break
			for tok in sent.tokens():
				form = unicode(tok.orth())
				if not case_sens:
					form = form.lower()
				forms[form] = forms.get(form, 0) + 1 # inc count
				if self.treat_unknown:
					# is the token "unknown"?
					# that is unknown tag appears among its possible tags
					tok_unkmask = corpus2.mask_token(tok, self.unk_mask, False)
					if not tok_unkmask.get_masked(self.unk_mask).is_null():
						# non-empty intersection with the unknown tag
						# get the preferred disamb tag
						tagstr = self.tagset.tag_to_string(
							tok.get_preferred_lexeme(self.tagset).tag())
						unktags[tagstr] = unktags.get(tagstr, 0) + 1 # += 1
		del reader
		# get the most frequent
		if self.verbose:
			sys.stderr.write('Generating lexicon...\n')
		data = [(w, forms[w]) for w in forms if forms[w] >= min_freq]
		del forms # release loads of memory
		if max_entries > 0:
			# leave at most max_entries (preferring frequent ones)
			data = sorted(data, key=ig(1), reverse=True)[:max_entries]
		# get words only and save them to lexicon
		data = map(ig(0), data)
		with codecs.open(corpio.f_name(self.model_name, self.data_dir,
			config.EXT_LEX), 'wb', 'utf-8') as lex_file:
			for form in data:
				# write a mapping form->form so that each form contained
				# will be retained and the rest will be skipped
				lex_file.write(u'%s\t%s\n' % (form, form))
		
		# do similar thing with tags of unknown words
		wanted_unk = [t for t in unktags if unktags[t] >= unk_freq]
		with open(corpio.f_name(self.model_name, self.data_dir,
			config.EXT_UNKTAGS), 'w') as unk_file:
			for tagstr in wanted_unk:
				# just write the tag
				unk_file.write('%s\n' % tagstr)
		# memorise unktags as valid tags for usage during training
		self.unktags = [self.tagset.parse_simple_tag(t) for t in wanted_unk]


	def resolve_object_path(self, target_component):
		# if it is absolute path then ok
		if target_component and os.path.isabs(target_component):
			if os.path.exists(target_component):
				return target_component
			else:
				raise IOError('Cannot locate %s' % target_component)
		# if nothing was provided then looking for a directory
		if not target_component:
			current_dir = os.getcwd() # try current directory
			dirname, dirs, files = os.walk(current_dir).next()
			if os.path.exists(current_dir) and any(name.endswith(config.EXT_CR) for name in files):
				return current_dir
			default_dir = os.path.join(DEFAULT_DIR, 'model') # if current dir has no model files then defaults to installation path with 'model' subdir
			if os.path.exists(default_dir):
				return default_dir
		local_dir = os.path.join(os.getcwd(), target_component) # if something was provided (and is not absolute) then check it against current directory
		if os.path.exists(local_dir):
			return local_dir
		default_dir = os.path.join(DEFAULT_DIR, 'config', target_component) # try installation config directory
		if os.path.exists(default_dir):
			return default_dir
		default_dir = os.path.join(DEFAULT_DIR, 'model', target_component) # try installation model directory (actually subdirectory of installation/model/)
		if os.path.exists(default_dir):
			return default_dir
		raise IOError('Cannot locate %s. Search in:\n\t%s\n\t%s\n\t%s' % (target_component, os.getcwd(), os.path.join(DEFAULT_DIR, 'model'), os.path.join(DEFAULT_DIR, 'config')))

	
	def __is_unknown(self, tok):
		"""Return whether the token contains an "uknown word" tag."""
		# mask each tag with the unknown tag, get sum of that
		tok_unkmask = corpus2.mask_token(tok, self.unk_mask, False)
		# if masked value equals to unk_mask, we've got an unk tag there
		return tok_unkmask == self.unk_mask
		
	
	def _preprocess_unknown(self, sent):
		"""If self.treat_unknown, preprocesses words recognised as "unknown"
		to also contain tags from memorised "uknown word tag" list. This is to
		make the tagger able to guess the tags for unknown words even if the
		analyser has failed.
		
		"Unknown" words are first identified -- those are the tokens having
		the unknown word tag and possibly other tags. Then tags from our list
		are added to such tokens. It is assumed here that the training corpus
		contains reference morphological analysis for "known" words, while
		words not present in the morphological dictionary are assigned just the
		correct tag (marked as disamb) as well as non-disamb "unknown word"
		tag.
		Technically the process will result in tokens assigned the unknown tag
		plus tags from our list -- this way those unknown tokens may be
		identified be the classifier as belonging to a separate class.
		This procedure is to be triggered both during training and normal
		tagging -- to train and perform under similar conditions."""
		if self.treat_unknown:
			for tok in sent.tokens():
				if self.__is_unknown(tok):
					lemma_utf8 = unicode(tok.orth()).lower().encode('utf-8')
					disamb = tok.get_preferred_lexeme(self.tagset).tag()
					for unktag in self.unktags:
						if unktag != disamb:
							tok.add_lexeme(corpus2.Lexeme.create_utf8(lemma_utf8, unktag))
					tok.remove_duplicate_lexemes()
	
	def train_and_save(self, in_path, input_format):
		"""Trains the tagger and stores the model to files beginning with
		model_name."""
		# gather most frequent forms
		self._gather_lexicon(in_path, input_format)
		# set-up layers (couldn't do that before as lexicon was generated)
		self.layers = corpio.Layers(self.conf, self.model_name,
			self.conf_dir, self.data_dir)
		self.models = {}
		if self.verbose:
			sys.stderr.write('Generating training data...\n')
		# open files for storing training examples
		tr_files = classify.open_tr_files(
			self.model_name, self.data_dir, self.layers.layers)
		attr_met = set() # attr names where training data available, known
		# set-up the reader and gather feature values for subsequent sentences
		assert input_format != 'txt' and input_format != 'premorph', 'Cannot train using ' + input_format + ' format'
		reader = corpio.get_reader(in_path, self.tagset, input_format)
		
		self.stats.clear()
		
		while True:
			sent = reader.get_next_sentence()
			if not sent:
				break # end of input
			# replace artifical analysis of "unknown" with unk tags
			self._preprocess_unknown(sent)
			# if tagging rules provided, apply them
			if self.layers.tag_rules is not None:
				self.layers.tag_rules.execute_once(sent)
			# leave 1 disamb tag per token and
			# treat unspecified attributes as each value set
			# also memorise indices of unknown words as the unk tags will have
			# been removed when we come to deal with non-wordclass attrs
			unknown_at = []
			for tok in sent.tokens():
				any_disamb = corpus2.select_preferred_disamb_tag(self.tagset, tok)
				if self.layers.tag_rules is None:
					assert any_disamb # the rules could have discarded disambs
				corpus2.expand_optional_attrs(self.tagset, tok)
				unknown_at.append(self.treat_unknown and self.__is_unknown(tok))
			# prepare WCCL context
			con = corpio.create_context(sent)
			# iterate over attributes
			for attr_name, attr_mask, ops in self.layers.layers:
				# get file for storing training data
				tr_file = tr_files[attr_name]
				got_data = False
				# iterate over each sentence token
				for tok_idx, tok in enumerate(sent.tokens()):
					con.set_position(tok_idx) # for WCCL ops
					all_attr_vals = corpus2.mask_token(tok, attr_mask, False)
					num_attr_vals = corpus2.mask_card(all_attr_vals)
					disamb_attr_vals = corpus2.mask_token(tok, attr_mask, True)
					# may be that disamb_attr_vals.is_null(), as for some
					# attrs no value is given
					# get values of WCCL features and attr values
					feat_vals = [op.base_apply(con)
						.to_compact_string(self.tagset).decode('utf-8')
						for op in ops]
					class_label = corpio.mask2text(self.tagset, disamb_attr_vals)
					# generate training example and store to file
					classify.write_example(tr_file, feat_vals, class_label)
					self.stats.num_evals += 1
					got_data = (not disamb_attr_vals.is_null())
					if got_data:
						# remove lexemes with non-disamb attr values
						# must be equal to mask to be left
						corpus2.disambiguate_equal(tok, all_attr_vals, disamb_attr_vals)
				classify.write_end_of_sent(tr_file)
				if got_data: attr_met.add(attr_name) # TODO save it somehow!
			self.stats.num_sents += 1
			self.stats.num_toks += sent.tokens().size()
			if self.verbose: self.stats.maybe_report()
		del reader
		classify.close_tr_files(tr_files)
		
		# train the classifier for each layer
		for layer in self.layers.layers:
			attr_name = layer[0]
			if self.verbose:
				sys.stderr.write('Training classifier for %s... ' % attr_name)
			here_cr = None
			if attr_name in attr_met:
				here_cr = classify.train_and_save(
					self.conf, self.model_name,
					self.conf_dir, self.data_dir, attr_name)
			self.models[attr_name] = here_cr
			if self.verbose:
				sys.stderr.write('done.\n')
		
		if self.verbose:
			sys.stderr.write('Tagger trained.\n')
			self.stats.dump()
	
	def disambiguate_sentence(self, sent):
		"""Disambiguates a single sentence by removing contextually
		inapropriate interpretations. This will result in exactly one
		tag per token (there may be multiple lexemes if there is a lemma
		ambiguity)."""
		# if tagging rules provided, apply them
		if self.layers.tag_rules is not None:
			self.layers.tag_rules.execute_once(sent)
		# treat unspecified attributes as each value set
		# also memorise indices of unknown words as the unk tags will have
		# been removed when we come to deal with non-wordclass attrs
		unknown_at = []
		for tok in sent.tokens():
			corpus2.expand_optional_attrs(self.tagset, tok)
			unknown_at.append(self.treat_unknown and self.__is_unknown(tok))
		# proper disambiguation: iterate over attributes, then tokens
		for attr_name, attr_mask, ops in self.layers.layers:
			here_cr = self.models[attr_name]
			if here_cr is not None:
				# else there was no training data
				# prepare WCCL context and feed the sentence features
				con = corpio.create_context(sent)
				classify.open_sent(here_cr)
				for tok_idx, tok in enumerate(sent.tokens()):
					con.set_position(tok_idx)
					# get values of WCCL features and attr values
					feat_vals = [op.base_apply(con)
						.to_compact_string(self.tagset).decode('utf-8')
						for op in ops]
					classify.eat_token(here_cr, feat_vals)
				classify.close_sent(here_cr)
				# now take the decisions from CRF, decode them into tag choices
				for tok_idx, tok in enumerate(sent.tokens()):
					class_label = classify.classify_token(here_cr, tok_idx)
					# decode into mask, leave only satisfying lexemes
					all_attr_vals = corpus2.mask_token(tok, attr_mask, False)
					wanted_attr_vals = corpio.text2mask(self.tagset, class_label)
					if wanted_attr_vals.is_null():
						succ = False
					else:
						succ = corpus2.disambiguate_equal(
							tok, all_attr_vals, wanted_attr_vals)
						self.stats.num_evals += 1
						self.stats.layer_gets[attr_name] += 1
						if not succ:
							self.stats.layer_fails[attr_name] += 1
					#(tok, con, attr_name, attr_mask, ops, here_cr)
		# post-processing: force one tag per token if any ambiguities left
		# (including artificially introduced by coding of no-value optionals)
		for tok in sent.tokens():
			corpus2.select_singular_tags(self.tagset, tok) # fix optional attrs
			corpus2.select_preferred_tag(self.tagset, tok) # force one tag/tok
		self.stats.num_sents += 1
		self.stats.num_toks += sent.tokens().size()
		if self.verbose: self.stats.maybe_report()
	
	def tag_sentence(self, sent, preserve_ambiguity):
		"""Triggers disambiguation of the given sentence and, when
		preserve_ambiguity, restores the discarded lexemes as non-disamb."""
		self._preprocess_unknown(sent)
		if preserve_ambiguity:
			disd = sent.clone_shared()
			self.disambiguate_sentence(disd)
			for tok_tagd, tok_disd in zip(sent.tokens(), disd.tokens()):
				corpus2.set_disambs(tok_tagd, tok_disd.get_preferred_lexeme(self.tagset).tag())
		else:
			self.disambiguate_sentence(sent)
	
	def tag_input(self, in_path, out_path, input_format, output_format,
			preserve_chunks = False, preserve_ambiguity = False):
		"""Tags the input and writes processed input to out_path or stdout if
		out_path is None. Similarly, if in_path is None, will read stdin."""
		# set-up reader and writer and proceed with proper tagging
		reader = corpio.get_reader(in_path, self.tagset, input_format, self.maca_config)
		writer = corpio.get_writer(out_path, self.tagset, output_format)
		
		self.stats.clear()
		if preserve_chunks:
			while True:
				chunk = reader.get_next_chunk()
				if not chunk:
					break # end of input
				# process each sentence separately
				for sent in chunk.sentences():
					self.tag_sentence(sent, preserve_ambiguity)
				# save tagged input
				writer.write_chunk(chunk)
		else:
			while True:
				sent = reader.get_next_sentence()
				if not sent:
					break # end of input
				self.tag_sentence(sent, preserve_ambiguity)
				writer.write_sentence(sent)
		writer.finish()
		del reader
		if self.verbose:
			self.stats.dump()
