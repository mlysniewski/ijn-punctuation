#!/usr/bin/env python
# -*- coding: utf-8 -*-

#ngram-count -tolower -sort -order 3 -text 
#/Users/Marcin/Documents/Uczelnia/Semestr\ 10/
#Inżynieria\ Języka\ Naturalnego/kpwr-tags/00101665.txt 

import os
import sys
import subprocess
import optparse

def count_in_dir(order, dir):
	for root, sub_folders, files in os.walk(dir):
		for file in files:
			if file.endswith('txt'):
				path, filename = os.path.split(file)
				filename = filename.replace('.txt', '.count')
				subprocess.call(['ngram-count', '-tolower', '-sort', \
					'-order',  str(order), '-text', dir+file, '-write', \
					dir+filename])

def merge_counts(merged_file, dir):
	f = open(merged_file, 'w')
	f.close()
	tmp = merged_file + ".tmp"
	
	count_files = ""
	for root, sub_folders, files in os.walk(dir):
		for file in files:
			if file.endswith('count'):
				
				subprocess.call(['ngram-merge', '-write', tmp, '--', \
					dir+file, merged_file])
				fi1 = open(tmp, 'r')
				fi2 = open(merged_file, 'w')
				fi2.write(fi1.read())
				fi1.close()
				fi2.close()
				
	print count_files



def main():
	p = optparse.OptionParse()
	p.add_option("-d", "--directory",
		help="Directory of converted corpus")
	p.add_option("-o", "--output",
		help="File to which merged ngrams count are saved")
	p.add_option("-n", "--num",
		help="Order of ngrams to count from prepared files")
	(opt, args) = p.parse_args()

	if not opt.directory and not opt.output and not opt.num:
		print "All options are obligatory"
		sys.exit(1)

	n = int(opt.num)

	count_in_dir(n, opt.directory)
	merge_counts(opt.out, opt.directory)

if __name__ == "__main__":
	main()