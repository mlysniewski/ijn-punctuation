sufix ``-punc'' wskazuje na plik z poprawną interpunkcją.


lp |		Nazwa pliku			| Źródło	
******************************************************************************
01 | test1.txt					| Świat według clarksona, ,,Władcy Mórz? Dziś 
								| jesteśmy już tylko rozbitkami''
02 | test2.txt					| Andrzej Sapkowski, ,,Boży Bojownicy'', str100
03 | test3.txt					| Mario Puzo, ,,Ojciec Chrzestny'' str83
04 | test4.txt					| http://pl.wikipedia.org/wiki/Druga_wojna_%C5%9Bwiatowa
