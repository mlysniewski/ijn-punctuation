#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
from math import pow

class NGramModel(object):
	__prob = 'probability'
	__disc = 'discount'
	def __init__(self):
		self.num_ngrams = {}
		self.sequence_prob = {}

	def __read_ngrams(self, order, start, file_lines):
		num = self.num_ngrams[order]
		for i in xrange(start, num + start):
			line = file_lines[i]
			line = line.strip('\n')
			line_cont = line.split('\t')
			prob = float(line_cont[0])
			sequence = line_cont[1]
			disc = 0
			if len(line_cont) == 3:
				disc = float(line_cont[2])
			self.sequence_prob[sequence] = {self.__prob: prob, self.__disc: disc}

	"""
	Reads language model file in arpa format
	param file -- path to lm file in arpa format
	"""
	def read_lm(self, file):
		lm_file = open(file, 'r')

		ngram_section = False

		arpa_lines = lm_file.readlines()
		for i in range(len(arpa_lines)):
			line = arpa_lines[i]
			m = re.match(r"^ngram\s(\d)=(\d+)$", line)
			if m:
				self.num_ngrams[int(m.group(1))] = int(m.group(2))

			m = re.match(r".*(\d)(?:-grams:){1}$", line)
			if m:
				n = int(m.group(1))
				self.__read_ngrams(n, i+1, arpa_lines)
				i += self.num_ngrams[n] + 1

		lm_file.close()

	def logprob_sequence(self, word, sequence):
		query = " ".join(sequence) + " " + word if len(sequence) > 0 else word
		#print query
		i = 1
		disc_val = 0
		while not query in self.sequence_prob and i <= len(sequence):
			query = " ".join(sequence[i:len(sequence)]) + " " + word \
				if len(sequence) - i > 0 else word 
			#print query
			disc = " ".join(sequence[i - 1:len(sequence)])
			disc_val += self.sequence_prob[disc][self.__disc] \
				if disc in self.sequence_prob else 0
			#print disc
			#print disc_val
			i += 1

		#print query
		logprob = self.sequence_prob[query][self.__prob] + disc_val \
			if query in self.sequence_prob else -float("inf")

		return logprob

	def prob_sequence(self, word, sequence):
		return pow(10, self.logprob_sequence(word, sequence))

def main():
	model = NGramModel()
	model.read_lm("ijn-model.lm")
	logprob = model.logprob_sequence("subst:pl:inst:n", ["subst:sg:acc:m1", "subst:sg:inst:f"])
	print logprob
	print model.prob_sequence("aglt:sg:pri:imperf:nw24k", ["subst:sg:nom:f"])

if __name__ =="__main__":
	main()