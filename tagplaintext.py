#!/usr/bin/env python
# -*- coding: utf-8 -*-

import corpus2
from wcrft import tagger

import os
import codecs

_input_format = "txt"
_output_format = "xces"
_tagset = "nkjp"
_tmp_out = "tmp.xml"

_config_path = "/home/ijn/apps/wcrft/wcrft/config/nkjp_s2.ini"
_model_path = "/home/ijn/apps/model_nkjp10_wcrft_s2/"
_tagger = tagger.Tagger(_config_path, _model_path)
_tagger.load_model()

def _is_aglt(ctag):
	return 'aglt' in ctag

def _chunks(reader):
	while True:
		chunk = reader.get_next_chunk()
		if not chunk:
			break;
		yield chunk	

def read_tagged_file(path):
	tagset = corpus2.get_named_tagset(_tagset)
	reader = corpus2.TokenReader.create_path_reader(_output_format, tagset, path)
	zdania = []
	for chunk in _chunks(reader):
		for sent in chunk.sentences():
			zdanie=[]
			for tok in sent.tokens():
				#print tok.orth_utf8()
				token = {}
				token['orth'] = tok.orth_utf8()
				token['base'] = tok.lexemes()[0].lemma_utf8()
				token['ctag'] = tagset.tag_to_string(tok.lexemes()[0].tag())
				#for lex in tok.lexemes():
				#	print '  '+lex.lemma_utf8()
				#	print '  '+tagset.tag_to_string(lex.tag())
				if token['ctag']=='interp':
					pass
				elif _is_aglt(token['ctag']):
					zdanie[-1]['orth'] += token['orth']
				else:
					zdanie.append(token)
			zdania.append(zdanie)
	return zdania

def tag_plaintext(file):
	_tagger.tag_input(file, _tmp_out, _input_format, _output_format, True)
	sentences = read_tagged_file(_tmp_out)
	os.remove(_tmp_out)
	f = open(file, 'r')
	return f.read(), sentences


def main():
	plaintext, tagged = tag_plaintext("test.txt")
	print plaintext
	print tagged

if __name__ == "__main__":
	main()
